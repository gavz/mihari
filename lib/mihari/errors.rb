# frozen_string_literal: true

module Mihari
  class Error < StandardError; end
end
